var gulp = require('gulp'),
    watch = require('gulp-watch'),
    fileInclude = require('gulp-file-include'),
    rename = require('gulp-rename'),
    sourcemaps = require('gulp-sourcemaps'),
    htmlmin = require('gulp-htmlmin'),

    // Styles
    sass = require('gulp-sass'),
    cleanCSS = require('gulp-clean-css'),

    // Scripts
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify');

var srcFolder = './src',
	distFolder = './assets',
	htmlFolder = '';

var assets = {
				source : {
					sass: [
							srcFolder + '/sass/theme.scss'
							],
					styles: [
								'./bower_components/bootstrap/dist/css/bootstrap.min.css',
								srcFolder + '/styles/theme.css'
							],
					scripts: [
								'./bower_components/jquery/dist/jquery.js',
								'./bower_components/bootstrap/dist/js/bootstrap.js',
								srcFolder + '/scripts/common.js'
							],
					html: srcFolder + '/html/*.html',
					htmlInclude: srcFolder + '/html/includes/*.htm',
					images: srcFolder + '/images/*'
				},
				destination : {
					sass: srcFolder + '/styles/',
					scripts: distFolder + '/scripts/',
					styles: distFolder + '/styles/',
					html: htmlFolder,
					images: distFolder + '/images/',
				},
				file : {
					css: 'style.css',
					js: 'script.js'
				}
			};


gulp.task('default', ['watch-sass', 'compile-sass', 'compile-style', 'compile-html', 'compile-script', 'compile-images'],function(){

	watch(srcFolder + '/sass/modules/*.scss', ['compile-sass'] , function(){
		gulp.start('watch-sass')
	});

	watch(assets.source.sass, function(){
		gulp.start('compile-sass')
	});

	watch(assets.source.styles, function(){
		gulp.start('compile-style')
	});

	watch([assets.source.html, assets.source.htmlInclude], function(){
		gulp.start('compile-html')
	});

	watch(assets.source.scripts, function(){
		gulp.start('compile-script')
	});

	watch(assets.source.images, function() {
		gulp.start('compile-images')
	})
});

gulp.task('compile-sass', function() {
	var  errorHandle = function(error) {
		this.emit('end');
	};

	gulp.src(assets.source.sass)
		.pipe(sass({outputStyle: 'expanded'})).on('error', errorHandle)
		.pipe(sourcemaps.write('.', { sourceRoot: 'css-source' }))
		.pipe(gulp.dest(assets.destination.sass));
});

gulp.task('watch-sass', function(){
	gulp.watch(srcFolder + '/sass/modules/*.scss', ['compile-sass']);
});

gulp.task('compile-style', function() {
	gulp.src(assets.source.styles)
		.pipe(concat(assets.file.css))
		.pipe(cleanCSS({keepSpecialComments : 0}))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(assets.destination.styles));
});

gulp.task('compile-html', function(){
	gulp.src(assets.source.html)
		.pipe(fileInclude())
		.pipe(htmlmin({collapseWhitespace: true, conservativeCollapse: true}))
		.pipe(gulp.dest(assets.destination.html));
});

gulp.task('compile-script', function(){
	gulp.src(assets.source.scripts)
		.pipe(concat(assets.file.js))
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(assets.destination.scripts));
});

gulp.task('compile-images', function(){
	gulp.src(assets.source.images)
		.pipe(gulp.dest(assets.destination.images));
});