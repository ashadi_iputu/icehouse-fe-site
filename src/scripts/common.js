var DATA_LIST,
	NUM_DATA = 6,
	COUNT_LIST = 5;

$(document).ready(function() {
	initNavbar();
	initData();
	resizeWindow();
	paginationEvent();
	openModalItem();
});

function resizeWindow() {
	$(window).resize(function(event) {
		if($(window).outerWidth() > 767) {
			renderHeightListItem();
			resetNavBar();
		}
		else
			resetHeightListItem();
	});
}

function initNavbar() {
	$('.navbar-nav li').first().addClass('active');

	$('.navbar-nav a').on('click', function(event) {
		
		var $this = $(this),
			$parent = $this.parent(),
			hrefVal = $this.attr('href');

		$('.navbar-nav  li').removeClass('active');
		$parent.addClass('active');

		if(hrefVal.indexOf('home') < 0) {
			templateName = hrefVal.substr(1, hrefVal.length);

			$('.section-content .load-content').html('Please wait...');
			$('.section-item-list').addClass('hide');
			$('.section-content').removeClass('hide');
			$('.section-content .load-content').load('templates/' + templateName + '.htm');
		}
		else {
			$('.section-content .load-content').html('Please wait...');
			$('.section-item-list').removeClass('hide');
			$('.section-content').addClass('hide');

			renderHeightListItem();
		}

		if($(window).width() < 768) {
			$('.navbar-toggle').trigger('click');
		}
	});

	$('.navbar-toggle').on('click', function(event) {
		event.preventDefault();

		var $this = $(this),
			$navbar = $('.navbar-collapse'),
			leftPos = 0,
			opacityVal = 1;
		
		if($this.hasClass('open')) {
			leftPos = -($navbar.width());
			opacityVal = 0;

			$('body').removeClass('open-nav');
			$this.removeClass('open');
			$navbar.removeClass('open');
		}
		else {
			$('body').addClass('open-nav');
			$this.addClass('open');
			$navbar.addClass('open');
		}
		
		
		$navbar.animate({
			left: leftPos
		},
			'fast'
		);

		$('.navbar-overlay').css('display', (opacityVal > 0 ? 'block' : 'none'));
		$('.navbar-overlay').animate({
			opacity: opacityVal
		},
			'fast'
		);
	});
}

function resetNavBar() {
	$('body').removeClass('open-nav');

	$('.navbar-overlay').css({
		'display': 'none',
		'opacity': 0
	});

	$('.navbar-collapse').css({
		'left': ''
	});

	$('.navbar-toggle').removeClass('open');
	$('.navbar-collapse').removeClass('open');
}

function initData() {
	$.ajax({
		url: 'http://jsonplaceholder.typicode.com/posts',
		type: 'GET',
		dataType: 'json'
	})
	.done(function(response) {
		$('.preloader').addClass('hide');
		$('.preloader').prev().removeClass('hide');
		DATA_LIST = response;
		generateDataListItem(DATA_LIST, 1, NUM_DATA);
		generateListOfPage(DATA_LIST, NUM_DATA, 1, COUNT_LIST);
		setActivePagination(1);
	})
	.fail(function() {
		alert("List items error, list items cannot retrieved");
	});
	
}

function generateDataListItem(jsonData, page, numData) {
	renderListItem(generateData(jsonData, page, numData));

	if($(window).width() >= 768)
		renderHeightListItem();
}

function generateData(jsonData, page, numData) {
	var startIndex = (page-1)*numData,
		endIndex = page * numData,
		pageData = jsonData.slice(startIndex, endIndex);

	return pageData;
}

function renderListItem(jsonData) {
	var $item = $('.item-master'),
		$listContainer = $('.list-container');

	$('.item-clone').remove();

	$.each(jsonData, function(index, val) {

		var $itemClone = $item.clone(),
			$itemThumnail = $itemClone.find('.thumbnail-list'),
			$itemTitle = $itemClone.find('.caption-title h2'),
			$itemBody = $itemClone.find('.caption p.body'),
			$itemBtn = $itemClone.find('.caption p a');

		$itemThumnail.attr('id', 'item-' + val.id);
		$itemBtn.data('id', val.id);
		$itemBtn.attr('data-page', val.id);
		$itemTitle.text(val.title);
		$itemBody.text(val.body);

		$itemClone.addClass('item-clone');
		$itemClone.removeClass('item-master');
		$itemClone.removeClass('hide');

		$listContainer.append($itemClone);
	});
}

function renderHeightListItem() {
	var maxHeight = 0;

	resetHeightListItem();

	$('.item-clone .thumbnail-list').each(function(index, el) {
		var $this = $(this),
			thisHeight = $this.height();

		maxHeight = thisHeight >= maxHeight ? thisHeight : maxHeight;
	});

	$('.item-clone .thumbnail-list').height(maxHeight);
}

function resetHeightListItem() {
	$('.item-clone .thumbnail-list').css('height', '');
}

function openModalItem() {
	$('.modal-item').modal({
        backdrop: 'static',
        show: false
    });

	$('body').on('click', '.thumbnail-list .btn-container a', function(event) {
		event.preventDefault();
		
		var $this = $(this),
			postId = $this.data('id'),
			$commentMaster = $('.modal-item .comment-list-master'),
			$modalBody = $('.modal-item .modal-body');
		
		$('.comment-list').remove();
		$('.comment-preloader').removeClass('hide');
		$('.modal-item').modal('show');

		$.ajax({
			url: 'http://jsonplaceholder.typicode.com/posts/'+ postId +'/comments',
			type: 'GET',
			dataType: 'json'
		})
		.done(function(response) {
			
			$.each(response, function(index, val) {
				var $commentClone = $commentMaster.clone(),
					$commentName = $commentClone.find('.comment-name'),
					$commentEmail = $commentClone.find('.comment-email'),
					$commentBody = $commentClone.find('.comment-body');
				
				$commentClone.attr('id', 'comment-' + val.id);
				$commentName.text(val.name);
				$commentEmail.text(val.email);
				$commentBody.text(val.body);

				$commentClone.addClass('comment-list');
				$commentClone.removeClass('comment-list-master');
				$commentClone.removeClass('hide');

				$modalBody.append($commentClone);
			});

			$('.comment-preloader').addClass('hide');

		})
		.fail(function() {
			alert("List comments error, list comment cannot retrieved");
		});
		
		
	});
}

function paginationEvent() {
	$('body').on('click', '.pagination a', function(event) {
		event.preventDefault();
				
		var $this = $(this);

		if($this.parent().hasClass('disabled')) 
			return;

		if($this.hasClass('page-number')) {
			pageNumber = parseInt($this.data('page'));
			generateDataListItem(DATA_LIST, pageNumber, NUM_DATA);
			setActivePagination(pageNumber);
		}
		else {
			if($this.hasClass('page-nav')) {
				indexList = parseInt($this.data('list'));
				generateListOfPage(DATA_LIST, NUM_DATA, indexList, COUNT_LIST);
				
				if($this.hasClass('page-prev'))
					pageNumber = indexList * COUNT_LIST;
				else 
					pageNumber = (indexList - 1) * COUNT_LIST + 1;

				generateDataListItem(DATA_LIST, pageNumber, NUM_DATA);
				setActivePagination(pageNumber);
			}
		}

		$(window).scrollTop($('.pagination').offset().top);
	});
}

function generateListOfPage(jsonData, numData, indexList, countList) {
	var countPage = Math.ceil(jsonData.length / numData),
		startIndex = (indexList - 1) * countList + 1;
		endIndex = indexList*countList,
		endIndexPage = endIndex > countPage ? countPage : endIndex,
		$pageNumber = $('.pagination .page-master');

		$('.page-clone').remove();
		
		for(index = startIndex; index<=endIndexPage; index++) {
			var $pageClone = $pageNumber.clone(),
				$pageAnchor = $pageClone.find('a');

			$pageAnchor.text(index);
			$pageAnchor.attr({
				'href': '#' + index,
				'data-page': index
			});
			 
			$pageAnchor.data('page', index);
			$pageClone.addClass('page-clone');
			$pageClone.removeClass('page-master');
			$pageClone.removeClass('hide');
			$pageClone.insertBefore('.pagination .next');
		}

		$('.pagination .page-prev').data('list', (indexList-1) );
		$('.pagination .page-next').data('list', (indexList+1) );

		$('.pagination .prev').removeClass('disabled');
		$('.pagination .next').removeClass('disabled');

		if((indexList-1) <= 0)
			$('.pagination .prev').addClass('disabled');

		if(endIndexPage == countPage)
			$('.pagination .next').addClass('disabled');
}

function setActivePagination(pageNumber) {
	$('.pagination li').removeClass('active');
	$('a[data-page="'+ pageNumber +'"]').parent().addClass('active');
}